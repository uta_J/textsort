package com.john.ConsoleSort;

import java.io.*;
import java.util.ArrayList;
import java.util.Arrays;

//������: ������ Sort
//
//        �������� ���������� ���������, ������� �� ����������� ����� �������� �� �� ����������� ���� �� ��������.
//
//        �������� ����������:
//          ��������� ������ ������������ ������� ��� ����������


public class ConsoleSort {
    public static void main(String[] args) {
        if(args.length != 1){
            System.err.println("To TextSort you must have 1 argument. Example: java ConsoleSort 'file_to_process'");
        }else{
            System.out.println("Starting program...");

            File file = null;

            try{
                file = new File(args[0]);
                if(!file.canRead())
                    throw new Exception();
            }catch(Exception ex){
                System.err.println("Can't read the file!");
            }
            sortText(file);

            System.out.println("Thanks for using ConsoleSort!");
        }
    }

    private static void sortText(File file){
        ArrayList<String> strings = fileReader(file);
        if(strings != null){

            System.out.println("English words: ");

            for(char c = 'a'; c <= 'z'; c++){
                for (String string : strings) {
                    if(string.toCharArray().length != 0){
                        if (string.charAt(0) == c) {
                            System.out.println(c + " - " + string);
                        }
                    }
                }
            }

            System.out.println("Russian words: ");

            for(char c = '�'; c <= '�'; c++){
                for (String string : strings) {
                    if(string.toCharArray().length != 0){
                        if (string.charAt(0) == c) {
                            System.out.println(c + " - " + string);
                        }
                    }
                }
            }
        }
    }

    private static ArrayList<String> fileReader(File file){
        System.out.println("Processing file...");
        try {

            BufferedReader br = new BufferedReader(new InputStreamReader(new FileInputStream(file)));

            ArrayList<String> strings = new ArrayList<>();
            String[] array;
            String line;

            while((line = br.readLine()) != null){
                array = line.toLowerCase().replace(',', ' ').replace('.', ' ').split(" ");
                strings.addAll(Arrays.asList(array));
            }

            if(strings.size() == 0)
                return null;
            else
                return strings;
        } catch (IOException e) {
            e.printStackTrace();
        }

        return null;
    }
}
